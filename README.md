# CI planning

JetFactory is intended to be a "universal" rootfs bootstrap tools depending on already avalaible packages. Configurations are the heart of it.
As such this part needs some development effort and possibly refactoring, so that users can use their own specific configurations.

## Jet-Factory and packages pipeline

For switchroot projects and platform such as the Tegra X1, packages tweaking is mandatory.

The pipeline should look like this :

Packages changes -> Build updated packages/error check -> Push to packages repository -> Rebuild rootfs -> Push to mirror

A few variables should be take in account in the pipeline/build process, mainly having the ability to made/ship variants.

Variants include :

- Shipping some pre-made kernel configuration ( for example : minimal, fedora, security focused, virtualsiation focused etc.. ) would be a must
- Shipping different Desktop Environment ( or no desktop environment ) flavored build

## Gentoo, NetBSD, Alpine

As those three OSes have their own requirements, we should setup a different CI ( maybe ) for them OR think of a reasonable way to integrate them to JetFactory.

## Android, MaruOS and variants

Android and variants differs completely from the rest of the GNU/Linux and BSD world, thus, a CI to  provide seamingless portgae of Android variants or android itself should be thought of as well.
